---
title: Forgejo monthly update - June 2023
publishDate: 2023-07-07
tags: ['news', 'report']
excerpt: Forgejo v1.20 is around the corner. Release candidates were published and most of the activity went into testing it, updating the documentation and rebuilding the release process. Forgejo Actions is now used for all Forgejo development and releases in a spirit of dogfooding. A hardware failure and a Denial of Service (DoS) attack disrupted the work during a week but did not break anything.
---

Forgejo v1.20 is around the corner: [release candidates](../2023-06-10-release-v1/) were published and most of the activity went into testing them, updating the documentation and rebuilding the release process. Although `Forgejo Actions` is not yet considered production ready, it proved stable enough to be used for all Forgejo development and releases. It did not always go smoothly and there were challenging times when a hardware failure and a Denial of Service (DoS) attack disrupted the work during a week.

Forgejo does not do much of anything in terms of communication but [got its first Wikipedia page](https://de.wikipedia.org/wiki/Forgejo) and was mentioned in the [State of the Forge Federation: 2023 edition](https://forgefriends.org/blog/2023/06/21/2023-06-state-forge-federation/). There is a great need for contributors who have strong skills that do not involve writing code, even beyond spreading the word about Forgejo, such as writing documentation, organizing video-conferences, etc. [Codeberg allocated funds](https://codeberg.org/forgejo/sustainability/issues/9#issuecomment-956394) to Forgejo that could be used to compensate contributors willing to help but who cannot afford to be volunteers.

### Development

#### Forgejo v1.20

The first [release candidates](../2023-06-10-release-v1/) for Forgejo v1.20 were published. The highlights are:

- The [internal CI/CD](/docs/v1.20/user/actions) known as `Forgejo Actions` is now used by [Forgejo itself](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/.forgejo/workflows) and the [Forgejo Runner](https://code.forgejo.org/forgejo/runner/src/branch/main/.forgejo/workflows) for testing pull requests and publishing releases. It is still in beta stage and disabled by default but it is stable and secure enough to be activated on [Forgejo's own instance](https://code.forgejo.org). An extensive [admin](/docs/v1.20/admin/actions) and [user](/docs/v1.20/user/actions) documentation is available.
- The User Interface (UI) and User eXperience (UX) changed significantly and will require some adjustment from users who will have to adapt to a different layout. And admins who created their own templates and styles will need to figure out, by reading the sources, how it evolved.
- New API endpoints were added (activity feeds, renaming users, uploading file, retrieving commits, etc.).

The [draft release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#draft-1-20-0-0) are mostly complete but they still work and contributions would be most welcome.

Publishing one of the release candidates was made specially difficult because Codeberg suffered from a hardware failure that was followed by a DoS attack. It took about a week instead of a few hours. A similar situation happened in the early days of Forgejo, when the first release was only half published because the process unexpectedly broke at the wrong time. The release process was refactored on that occasion to be resilient to network and hardware failures. This effort paid off this time around, when the release process had to be restarted no less than a dozen times over three days until it finally succeeded.

### Moderation

An [API was implemented](https://codeberg.org/forgejo/forgejo/pulls/827) to manage blocked users in organizations and user accounts.

### Federation

When artifacts (repositories, issues, etc.) are imported in Forgejo, they are now associated with a user [that acts as a placeholder if it does not already exists](https://codeberg.org/forgejo/forgejo/pulls/943). If that same user authenticate themselves on Forgejo via OAuth at a later time, [this placeholder will be promoted to a real user](https://codeberg.org/forgejo/forgejo/pulls/934).

In other words, if a repository that belongs to Jane Doe is federated from GitLab to a Forgejo instance, she will be able to reclaim it as soon as she registers via OAuth2 on the Forgejo instance. The OAuth2 authorization from GitLab is proof enough that she is the legitimate owner of this repository.

### Documentation

The Forgejo documentation is a patchwork from various sources (Codeberg, Gitea, etc.) as well as content authored by Forgejo contributors. To improve the documentation for the Forgejo v1.20 release a full pass was done to get relevant updates from these sources. In addition a new section was created with [Recommended Settings and Tips](https://forgejo.org/docs/v1.20/admin/recommendations/).

The `Forgejo Actions` [admin](https://forgejo.org/docs/v1.20/admin/actions/) guide is complete and the [user](https://forgejo.org/docs/v1.20/user/actions/) guide got better but still requires a significant work to be finished. An effort is made to include [examples](https://code.forgejo.org/actions/setup-forgejo/src/branch/main/testdata) that are part of the CI of actively maintained repositories so they can be verified to work.

### Forgejo Actions

A [new version of the Forgejo runner](https://code.forgejo.org/forgejo/runner/releases/tag/v2.1.0) was published to fix bugs and security issues. Although it is stable and runs for weeks uninterrupted, it is best confined on an isolated machine that is reset on a regular basis for security reasons.

There has been no report of security issues when enabling actions in Forgejo v1.19 and [Codeberg decided to enable it](https://codeberg.org/forgejo/discussions/issues/36#issuecomment-935435). That allowed for a simpler release process based on Forgejo Actions for both Forgejo and the `Forgejo runner` to be [implemented](https://codeberg.org/forgejo/website/pulls/230). It was used to publish all release candidates in the [experimental](https://codeberg.org/forgejo-experimental/) organization and helped fine tune it.

The [Woodpecker CI release process](https://codeberg.org/forgejo/forgejo/src/branch/v1.19/forgejo/releases) was still used to publish the [latest v1.19.4-0](https://codeberg.org/forgejo/forgejo/releases/tag/v1.19.4-0) release. It was retired and [moved to its own repository](https://codeberg.org/forgejo-contrib/forgejo-ci-woodpecker) as of Forgejo v1.20.

## Governance and communication

### Wikipedia

Earlier than expected, [Forgejo got its first Wikipedia page](https://de.wikipedia.org/wiki/Forgejo). It still needs secondary sources to be inserted to durably establish its notoriety but it has not yet been challenged and those can easily be added.

### State of the Forge Federation: 2023 edition

In the [State of the Forge Federation: 2023 edition](https://forgefriends.org/blog/2023/06/21/2023-06-state-forge-federation/) published 21 June Forgejo plays a central role.

> Late 2022 Forgejo, a new forge with a focus on federation was created. Dozens of people contributed to its making and it is now used in production by ten of thousands of users at Codeberg, Disroot, etc. This large and unforeseen undertaking diverted the energy of most contributors set to work on federation features [...]. [...] the foundations on which forge federation is being built have shifted and there is reason to hope it was for the best.

### Matrix archive bot

The privacy expectations of people visiting the [Forgejo chatrooms](https://matrix.to/#/#forgejo:matrix.org) is not high: they are fully aware they are publicly available and that anyone can visit them.

But even in this context the sudden appearance of an archive bot from Matrix.org surprised a number of active participants. The bot was banned while [the situation was discussed](https://codeberg.org/forgejo/discussions/issues/37). A few weeks later [Matrix.org shut down this service](https://codeberg.org/forgejo/discussions/issues/37#issuecomment-953680). Forgejo community members were likely not the only ones to protest.

### Governance

There has been some discussions about how teams are useful and why but no significant progress was made on governance otherwise.

## Hardware

In addition to the hosting provided by Codeberg, Forgejo needs a very secure hardware to host the cryptographic keys used to sign releases as well as the most sensitive work of the security team. It also occasionally needs to run resource consuming Continuous Integration jobs, for instance when performing end to end testing to verify a release can actually be used in a production environment. Or when hardware emulation is required to verify multi-architecture binaries run as expected.

A [new hardware](https://www.hetzner.com/dedicated-rootserver/ex101) was acquired and is being configured. It suffered intermittent failures that took significant time to figure out during the first week and was eventually replaced. These investigations were an opportunity to confirm that the stack on which Forgejo hardware is deployed is solid (Debian GNU/Linux and LXC). There was little doubt about it since Codeberg uses the same. But when a machine reboots randomly multiple times per day for not apparent reason and all hardware tests confirm it should not, it is a motivation to doubt about everything.

The [security team](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#security) now uses resources hosted behind a VPN at `octopuce.forgejo.org` to work together in a secure environment.

## Roadmap

Forgejo is still in the process of [defining its roadmap](https://codeberg.org/forgejo/discussions/issues/17), a concrete strategy to move forward with federation, scaling and robustness.

The [NLnet grant](https://codeberg.org/forgejo/sustainability/issues/1) that was awarded last month demanded a [workplan](https://codeberg.org/forgejo/sustainability/src/branch/main/2022-12-01-nlnet/2023-06-workplan.md). This provides some clarity about what the beneficiaries are set to accomplish in the following areas:

- UI and accessibility improvements
- DNS-Update-Checker RFC
- Cleaner Webhook system
- A new continuous integration agent
- Tools to produce a source distribution and multi-architecture Forgejo binaries
- An integrated release pipeline based on Forgejo

It is not a roadmap just yet, but it can help create one.

## Funding

During its General Assembly, Codeberg decided to [allocate funds to Forgejo](https://codeberg.org/forgejo/sustainability/issues/9#issuecomment-956394) and ideas are welcome to decide how they should be spent.

A [funding opportunity was discovered](https://codeberg.org/forgejo/sustainability/issues/13) to raise as much as 600K€ with an amount of administrative work well suited for small companies. No application was sent by the deadline (6 July). However there is [another opportunity](https://codeberg.org/forgejo/sustainability/issues/14), with a 31 July deadline.

## We Forge

Forgejo is a **community of people** who contribute in an inclusive environment. We forge on an equal footing, by reporting a bug, voicing an idea in the chatroom or implementing a new feature. The following list of contributors is meant to reflect this diversity and acknowledge all contributions since the last monthly report was published. If you are missing, please [ask for an update](https://codeberg.org/forgejo/website/issues/new).

- https://codeberg.org/alex19srv
- https://codeberg.org/Beowulf
- https://codeberg.org/caesar
- https://codeberg.org/circlebuilder
- https://codeberg.org/cmonty14
- https://codeberg.org/crystal
- https://codeberg.org/dachary
- https://codeberg.org/DanielGibson
- https://codeberg.org/DansLeRuSH
- https://codeberg.org/Dirk
- https://codeberg.org/dumblob
- https://codeberg.org/earl-warren
- https://codeberg.org/Fl1tzi
- https://codeberg.org/fluzz
- https://codeberg.org/fnetX
- https://codeberg.org/fr33domlover
- https://codeberg.org/GamePlayer-8
- https://codeberg.org/grosmanal
- https://codeberg.org/Gusted
- https://codeberg.org/helge
- https://codeberg.org/hrnz
- https://codeberg.org/JohnWalkerx
- https://codeberg.org/KaKi87
- https://codeberg.org/linos
- https://codeberg.org/macfanpl
- https://codeberg.org/maralorn
- https://codeberg.org/meaz
- https://codeberg.org/Mikaela
- https://codeberg.org/n0toose
- https://codeberg.org/Nulo
- https://codeberg.org/oewbgoieqwb
- https://codeberg.org/oliverpool
- https://codeberg.org/RaptaG
- https://codeberg.org/samrland
- https://codeberg.org/update.freak
- https://codeberg.org/viceice
- https://codeberg.org/xy

A **minority of Forgejo contributors earn a living** by implementing the roadmap co-created by the Forgejo community, see [the sustainability repository](https://codeberg.org/forgejo/sustainability) for the details.
