---
layout: '~/layouts/Markdown.astro'
publishDate: 'May 2023'
title: 'Forgejo FAQ'
---

For a FAQ on how Forgejo and Gitea are related, see the [dedicated comparison page](../compare/#faq).

## Where does the name come from?

**Forgejo** ([pronounced /forˈd͡ʒe.jo/](/static/forgejo.mp4)) is inspired by <i lang="eo">forĝejo</i>, the Esperanto word for forge.

## Who owns the Forgejo domains and trademarks?

The Forgejo domains are in the custody of [Codeberg e.V.](https://docs.codeberg.org/getting-started/what-is-codeberg/#what-is-codeberg-e.v.%3F) a non-profit based in Germany and dedicated to [hosting Free Software projects](https://codeberg.org/) since 2019. It is ultimately in control of Forgejo and [its bylaws](https://codeberg.org/Codeberg/org/src/branch/main/en/bylaws.md) guarantee it will keep further the interest of the general public. No trademark was registered at this point in time.

## What is 'Codeberg e.V.'?

[Codeberg e.V.](https://codeberg.org/Codeberg/org/src/branch/main/Imprint.md) is an association registered in Berlin, Germany. The **e.V.** abbreviation is for _eingetragener Verein_, which translates as 'registered association'. The link above is to the Impressum, which includes the required contact information, statements regarding non-profit status, and other helpful information. You may verify this information via the [German Federal Registration portal](https://www.handelsregister.de/rp_web/normalesuche.xhtml); just type in `Codeberg e.V.` as the company, click the Find button, and select the SI link for an xml structured report.

## Is Forgejo sustainable? How is it funded?

Sustaining Free Software projects developed in the interest of the general public is an ongoing challenge. Forgejo relies on a mixture of volunteer contributions, grants, donations and employee delegation to keep going. The details are documented transparently [in a repository dedicated to sustainability](https://codeberg.org/forgejo/sustainability).

## Is there a roadmap for Forgejo?

There is just one item on the roadmap: forge federation. [User research](https://codeberg.org/forgejo/user-research) is ongoing to build a roadmap that reflects actual user needs. If you are interested to participate, please [join the chat room](https://matrix.to/#/#forgejo-chat:matrix.org).

## Who is using Forgejo?

The largest known public instances are:

- [Codeberg](https://codeberg.org) with around 100,000 users and 80,000 projects as of November 2023
- [Disroot](https://git.disroot.org/)

See also the [list of instances](https://codeberg.org/forgejo-contrib/delightful-forgejo#public-instances) maintained in the page dedicated to Forgejo related resources.

## What is the governance of Forgejo?

Forgejo is a collective of individuals who [define their own governance](https://codeberg.org/forgejo/governance/src/branch/main/README.md). Its evolution is discussed [in a dedicated forum](https://codeberg.org/forgejo/discussions/issues) until an [decision is made](https://codeberg.org/forgejo/governance/src/branch/main/DECISION-MAKING.md) and [documented](https://codeberg.org/forgejo/governance/pulls). Codeberg e.V. does not run the day to day operations of Forgejo nor does it require the members of the project to follow its governance. It is solely responsible for ensuring the domains and trademarks are ultimately used to further the interest of the general public in accordance to its bylaws.

## Does Forgejo have a Code of Conduct?

Yes. The [Code of Conduct](https://codeberg.org/forgejo/code-of-conduct) applies to all spaces that are under the responsibility of the Forgejo project. The [moderation team](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#moderation) is available to resolve conflicts.

## Is Forgejo licensed under AGPL?

No. The [license of Forgejo](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/LICENSE) is MIT. It was suggested to change the license to AGPL in order to prevent a takeover from a for-profit company. It was however decided that the best way to prevent such a takeover was to have the Forgejo domains and trademarks in the custody of the Codeberg e.V. non-profit.

## How are security vulnerabilities handled?

Security issues are managed by [a team](/.well-known/security.txt) sharing the effort between Codeberg and Forgejo.
The security team is available at `security@forgejo.org` (GPG public key [1B638BDF10969D627926B8D9F585D0F99E1FB56F](https://keyoxide.org/security@forgejo.org)) and is tasked to identify, define, and respond to vulnerabilities.

## Why are container images published with the 1.19 tag?

The **1.19** tag is set to be the latest patch release, starting with [1.19.0-2](https://codeberg.org/forgejo/-/packages/container/forgejo/1.19.0-2). **1.19** will then be equal to **1.19.1-0** when it is released and so on. It can conveniently be used for automated upgrades to the latest stable release.

## Why is there no latest tag for container images?

Because upgrading from **1.X** to **1.X+1** (for instance from **1.18** to **1.19**) requires a [manual operation and human verification](/docs/latest/admin/upgrade). However it is possible to use the **X.Y** tag (for instance **1.19**) to get the latest point release automatically.

## What are the names of the built-in Forgejo themes?

Forgejo introduces two new themes: light version named **forgejo-light** and a dark version named **forgejo-dark**. They are the default for a new installation but will need to be set explicitly if the `app.ini` file already has a custom list of themes. For instance, if it looks like this:

```
[ui]
THEMES = gitea-auto,gitea-light,gitea-dark
```

the **forgejo-auto**, **forgejo-light** and **forgejo-dark** can be added as follows:

```
[ui]
THEMES = forgejo-auto,forgejo-light,forgejo-dark,gitea-auto,gitea-light,gitea-dark
```

## Why are there no mentions of mssql in the documentation? Or Windows binaries?

Although the Forgejo codebase is known to be easy to built for Windows
and reliably run with a mssql database, there currently is no
expertise on either of those in the Forgejo community. Maintaining a
distribution targetting a given database and Operating System requires
someone to build and maintain the release pipeline, figure out the
root cause of bugs and fix them.
